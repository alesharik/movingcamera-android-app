package com.alesharik.myapplication;

import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.NetworkInterface;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.concurrent.LinkedBlockingQueue;

public class MainActivity extends AppCompatActivity {
    private final Sender sender = new Sender();
    private final MagnetListener magnetListener = new MagnetListener();
    private final AccelListener accelListener = new AccelListener();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sender.start();
        SensorManager sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        sensorManager.registerListener(magnetListener, sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD), SensorManager.SENSOR_DELAY_GAME);
        sensorManager.registerListener(accelListener, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_GAME);

        final Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                handler.postDelayed(this, 300);
                float[] matrix = new float[9];
                SensorManager.getRotationMatrix(matrix, null, accelListener.getValues(), magnetListener.getValues());
                float[] angles = new float[3];
                SensorManager.getOrientation(matrix, angles);
                System.out.println(Arrays.toString(new double[]{angles[0] / Math.PI * 180, angles[1] / Math.PI * 180, angles[2] / Math.PI * 180}));
                sender.send(new DataRotate(angles[0] / Math.PI * 180, angles[1] / Math.PI * 180, angles[2] / Math.PI * 180));
            }
        }, 300);

        findViewById(R.id.top).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sender.send(new DataCamera(DataCamera.TOP));
            }
        });
        findViewById(R.id.right).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sender.send(new DataCamera(DataCamera.RIGHT));
            }
        });
        findViewById(R.id.left).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sender.send(new DataCamera(DataCamera.LEFT));
            }
        });
        findViewById(R.id.bottom).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sender.send(new DataCamera(DataCamera.BOTTOM));
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private static final class MagnetListener implements SensorEventListener {
        private float[] values = new float[] {0, 0, 0, 0, 0, 0, 0, 0, 0};

        @Override
        public void onSensorChanged(SensorEvent event) {
            values = event.values;
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {

        }

        public float[] getValues() {
            return values;
        }
    }

    private static final class AccelListener implements SensorEventListener {
        private float[] values = new float[] {0, 0, 0, 0, 0, 0, 0, 0, 0};

        @Override
        public void onSensorChanged(SensorEvent event) {
            values = event.values;
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {

        }

        public float[] getValues() {
            return values;
        }
    }

    private interface Data {
        byte[] toByteArray();
    }

    private static final class DataCamera implements Data {
        private static final ThreadLocal<ByteBuffer> BUF = new ThreadLocal<ByteBuffer>() {
            @Override
            protected ByteBuffer initialValue() {
                return ByteBuffer.allocateDirect(2);
            }
        };

        public static final byte LEFT = 1;
        public static final byte RIGHT = 2;
        public static final byte TOP = 3;
        public static final byte BOTTOM = 4;

        private final byte type;

        public DataCamera(byte type) {
            this.type = type;
        }

        @Override
        public byte[] toByteArray() {
            ByteBuffer byteBuffer = BUF.get();
            byteBuffer.position(0);
            byteBuffer.put((byte) 2);
            byteBuffer.put(type);
            byteBuffer.flip();
            byte[] ret = new byte[2];
            byteBuffer.get(ret);
            return ret;
        }
    }

    private static final class DataRotate implements Data {
        private static final ThreadLocal<ByteBuffer> BUF = new ThreadLocal<ByteBuffer>() {
            @Override
            protected ByteBuffer initialValue() {
                return ByteBuffer.allocateDirect(4 * 3 + 1);
            }
        };
        private final float azimuth;
        private final float pitch;
        private final float roll;

        public DataRotate(float azimuth, float pitch, float roll) {
            this.azimuth = azimuth;
            this.pitch = pitch;
            this.roll = roll;
        }

        public DataRotate(double v, double v1, double v2) {
            azimuth = (float) v;
            pitch = (float) v1;
            roll = (float) v2;
        }

        public byte[] toByteArray() {
            ByteBuffer byteBuffer = BUF.get();
            byteBuffer.position(0);
            byteBuffer.put((byte) 1);
            byteBuffer.putFloat(azimuth);
            byteBuffer.putFloat(pitch);
            byteBuffer.putFloat(roll);
            byteBuffer.flip();
            byte[] ret = new byte[4 * 3 + 1];
            byteBuffer.get(ret);
            return ret;
        }
    }

    private static final class Sender  extends Thread {
        private final LinkedBlockingQueue<Data> queue = new LinkedBlockingQueue<>();

        final static String INET_ADDR = "224.0.0.4";
        final static int PORT = 8888;

        @Override
        public void run() {
            try {
                MulticastSocket socket = new MulticastSocket(PORT);
                socket.setNetworkInterface(NetworkInterface.getByName("wlan0"));
                socket.joinGroup(InetAddress.getByName(INET_ADDR));
                while (!isInterrupted()) {
                    Data poll = queue.take();
                    byte[] bytes = poll.toByteArray();
                    DatagramPacket packet = new DatagramPacket(bytes, bytes.length, InetAddress.getByName(INET_ADDR), PORT);
                    socket.send(packet);
                }
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        public void send(Data data) {
            queue.add(data);
        }
    }
}
